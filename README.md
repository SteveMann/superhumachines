# Superhumachines

Super-human-machine intelligence, e.g. making cyborg technologies for superhuman intelligence.

## Support
Please view our website here: https://superhumachines.com

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
This course website is written on GitLab and you are invited to shape it and be part of this effort!

## Advisors
https://en.wikipedia.org/wiki/Thomas_A._Furness_III<br>
https://www.linkedin.com/in/paul-travers-534b336/<br>
https://caydenpierce.com/<br>

## Authors and acknowledgment
A list of those who have contributed to the project.<br>
https://gitlab.com/SteveMann<br>
https://gitlab.com/santiago.arciniegas<br>
https://gitlab.com/ptoone<br>

## License
This project follows a Copyleft approach with software licensed under a GNU General Public License v3.0

## Project status
Currently in development.

